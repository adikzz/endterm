FROM golang
RUN mkdir -p /adik/src/endterm/
WORKDIR /adik/src/endterm/
COPY . /adik/src/endterm/
CMD ["go", "run","./cmd/web"]