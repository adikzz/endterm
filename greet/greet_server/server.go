package main

import (
	"fmt"
	"log"
	"net"
	"time"

	"com.grpc.tleu/greet/greetpb"
	"google.golang.org/grpc"
)

type Server struct {
	greetpb.UnimplementedCalculatorServiceServer
}

func PrimeFactors(number int) (numbers []int) {
	for i := 2; i <= number/2; i++ {
		for number%i == 0 {
			numbers = append(numbers, i)
			number = number / i
		}
	}
	if number > 2 {
		numbers = append(numbers, number)
	}
	return numbers
}

func (s *Server) GreetManyTimes(req *greetpb.CalculatorManyTimesRequest, stream greetpb.CalculatorService_GreetManyTimesServer) error {
	fmt.Printf("GreetManyTimes function was invoked with %v \n", req)
	number := req.GetCalculator().GetNumber()
	numbers := PrimeFactors(int(number))
	for i := range numbers{
		res := &greetpb.CalculatorManyTimesResponse{Result: string(i)}
		if err := stream.Send(res); err != nil {
			log.Fatalf("error while sending greet many times responses: %v", err.Error())
		}
		time.Sleep(time.Second)
	}
	return nil
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	greetpb.RegisterCalculatorServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}
