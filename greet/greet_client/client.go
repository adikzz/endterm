package main

import (
	"com.grpc.tleu/greet/greetpb"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"io"
	"log"
)

func main() {
	fmt.Println("Hello I'm a client")

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := greetpb.NewCalculatorServiceClient(conn)
	doManyTimesFromServer(c)
}

func doManyTimesFromServer(c greetpb.CalculatorServiceClient) {
	ctx := context.Background()
	req := &greetpb.CalculatorManyTimesRequest{Calculator: &greetpb.Calculator{Number: int32(120)}}

	stream, err := c.GreetManyTimes(ctx, req)
	if err != nil {
		log.Fatalf("error while calling GreetManyTimes RPC %v", err)
	}
	defer stream.CloseSend()

LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				// we've reached the end of the stream
				break LOOP
			}
			log.Fatalf("error while reciving from CalculatorManyTimes RPC %v", err)
		}
		log.Printf("result is:%v \n", res.GetResult())
	}

}



